/**
 * This module handles connent display and sorting toggle buttons for the
 * LITK WordPress Test plugin.
 */

var LITK = LITK || {};

LITK.Seron = (function($) {

    /**
     * Delegated event handlers for content and sorting toggle buttons get
     * attached to the top-most Subpage list container. On click they will
     * call their respective handler functions.
     */
    function init() {

        var $subpagelist = $('.js-subpagelist-top');
        $subpagelist.on('click', '.js-toggle-button', _toggleContents);
        $subpagelist.on('click', '.js-sort-button', _sortTitles);
    }

    /**
     * This function toggles the display of the page contents and subpage
     * titles at the level of the clicked button.
     */
    function _toggleContents() {

        // the button that was clicked
        var $button = $(this);
        // the corresponding subpage contents
        var $contents = $button.siblings('ul').find('>li>p');
        // the titles of the subsubpages
        var $titles = $contents.siblings('.level');

        /**
         * DOM changes
         */

        // contents and titles display
        $contents.toggleClass('expanded');
        $titles.toggleClass('expanded');

        // button text
        if ($contents.hasClass('expanded')) {
            $button.text('collapse');
        } else {
            $button.text('expand');
        }
    }

    /**
     * This functions sorts the titles at the level of the clicked button.
     */
    function _sortTitles() {

        // the button that was clicked
        var $button = $(this);
        // the corresponding list
        var $ul = $button.siblings('ul');
        // the list's items to be sorted
        var $items = $ul.find('>li');

        /**
         * DOM changes
         */

        // sorting direction
        var ASC = 1;
        var DESC = -1;

        $ul.toggleClass('desc');
        var sorted = $ul.hasClass('desc') ? $items.sort(_sortDirection(ASC)) :
                                            $items.sort(_sortDirection(DESC));
        $ul.html(sorted);

        // button text
        if ($ul.hasClass('desc')) {
            $button.text('sort asc');
        } else {
            $button.text('sort desc');
        }
    }

    /**
     * This function returns a compare function to be used by the Array prototype
     * .sort() method. The ordering is based on the order parameter.
     *
     * @param  {Number} order - ASC (ascending) or DESC (descending)
     * @return {Function} - The compare function
     */
    function _sortDirection(order) {

        /**
         * This function takes HTML li elements and compares their children link
         * text contents. It returns a compare value used by the Array prototype
         * .sort() method.
         *
         * @param  {HTML li element}
         * @param  {HTML li element}
         * @return {Number} -1, 0, or 1 depending on the enclosing function order parameter.
         */
        return function _titleCompare(tagA, tagB) {

            // Strings extracted from link elements provided.
            var aTitle = $(tagA).children('a').text();
            var bTitle = $(tagB).children('a').text();

            if (aTitle < bTitle) {
                return order * 1;
            } else if (aTitle > bTitle) {
                return order * -1;
            } else {
                return 0;
            }
        };
}

    /**
     * The module API.
     *
     * This is used to activate the module.
     */
    return {
        activate: init
    };
})(jQuery);

// activate the module after document load
jQuery(document).ready(function documentReady() {

    LITK.Seron.activate();
});
