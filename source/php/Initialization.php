<?php namespace Seron;

/**
* This class initializes the plugin. It registers the plugin CSS, JS,
* shortcode and widget.
*/
class Initialization
{

    function __construct()
    {
        /** register CSS and JS */
        add_action('wp_enqueue_scripts', array($this, 'CSSJS'));
        /** register shortcode */
        add_shortcode(LITK_SHORTCODE, array($this, 'subpagesShortcode'));
        /** register widget */
        add_action('widgets_init', function()
            {
                register_widget('Seron\Lib\SubpagesWidget');
            });
    }

    /**
     * Adds plugin CSS and JavaScript.
     */
    public function CSSJS()
    {
        wp_enqueue_style('seron_css', plugins_url('/../css/style.css', __FILE__), NULL);
        wp_enqueue_script('seron_js', plugins_url('/../js/min/script-min.js', __FILE__), array('jquery'));
    }

    /**
     * Adds the plugin shortcode [litk­-wp-­test-seron­].
     *
     * The shortcode uses wp_list_pages() with an extended Walker_Page class to
     * provide featured images and page contents in addition to the tree-like structure
     * that the parent class generates.
     */
    public function subpagesShortcode()
    {
        // CSS class that marks the container of a subpage level.
        $list_level_marker = 'level';
        // Markup for the level expansion and sorting toggle buttons.
        $buttons = "<div class='button js-toggle-button'>expand</div>
                    <div class='button js-sort-button'>sort desc</div>";

        $args = array('child_of'        => get_the_ID(),
                      'echo'            => false,
                      'title_li'        => '',
                      'walker'          => new Lib\SubpagesWalker(),
                      'level_marker'    => $list_level_marker,
                      'buttons'         => $buttons);

        return "<div class='subpagelist-top js-subpagelist-top $list_level_marker'>" . $buttons .
               "<ul>" . wp_list_pages($args) . '</ul></div>';
    }
}
?>
