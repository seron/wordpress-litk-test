<?php namespace Seron\Lib;

/**
 * Adds SubpagesWidget widget by extending WP_Widget. It leverages the plugin
 * shortcode for its entire functionality.
 *
 * @see WP_Widget
 */
class SubpagesWidget extends \WP_Widget
{
    /**
     * Sets up the widgets name etc.
     */
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'litk-subpages-widget',
            'description' => 'Subpage tree structure.',
            );

        parent::__construct('litk-subpages-widget', 'LITK Subpage tree - Seron', $widget_ops);
    }

    /**
     * Outputs the content of the widget.
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        echo do_shortcode('[' . LITK_SHORTCODE . ']');
        echo $args['after_widget'];
    }
}

?>
