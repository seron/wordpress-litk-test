<?php
/*
Plugin Name: LITK WP Test
Description: This plugin displays the titles of any subpages relative to the currently displayed page. The subpages' titles are displayed in a tree-like structure, with only the next level being visible by default. Subpage titles are truncated to 20 characters. If present, a small thumbnail is displayed next to the title of each subpage. Each subpage level can be expanded and collapsed, revealing the page contents (without formatting) as well as titles of any subpages at the corresponding level. Content at each level may be sorted alphabetically by title in ascending and descending order. Shortcode: <strong>[litk­-wp-­test-seron­]</strong>, widget: <strong>LITK Subpage tree - Seron</strong>. Tested with WordPress 4.6, PHP 5.6.
Version:     1.0
Author:      Ivan Videsvall
*/

/** Sets up namespaces according to PSR-4 using composer. */
require(__DIR__ . '/vendor/autoload.php');

/** The shortcode name. */
define('LITK_SHORTCODE', 'litk­-wp-­test-seron­');

/** Register CSS, JS, shortcode and widget */
new Seron\Initialization();
?>
