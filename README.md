# README #

### Description ###
This plugin displays the titles of any subpages relative to the currently displayed page. The subpages' titles are displayed in a tree-like structure, with only the next level being visible by default. Subpage titles are truncated to 20 characters. If present, a small thumbnail is displayed next to the title of each subpage. Each subpage level can be expanded and collapsed, revealing the page contents (without formatting) as well as titles of any subpages at the corresponding level. Content at each level may be sorted alphabetically by title in ascending and descending order.

The functionality may be enabled both by widget as well as shortcode.

Tested with WordPress 4.6, PHP 5.6.

### Widget ###
LITK Subpage tree - Seron

### Shortcode ###
[litk­-wp-­test-seron­]

### Version ###
1.1

### Author ###
Ivan Videsvall

### Setup ###

* Download the plugin here: https://bitbucket.org/seron/wordpress-litk-test/downloads

* Unzip the file contents into the Wordpress plugin directory, typically wp-content/plugins. 

* Refresh the plugin page on the Wordpress dashboard.